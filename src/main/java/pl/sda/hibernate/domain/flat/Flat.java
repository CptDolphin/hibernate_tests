package pl.sda.hibernate.domain.flat;

import javax.persistence.*;
import java.util.List;

@Entity
public class Flat {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int surface;
    private String name;
    private String adress;
    private String ownerName;
    @OneToMany(mappedBy = "flat")//odwoluje sie do pola flat w room
    private List<Room> rooms;

    public Flat() {
    }

    public Flat(int surface, String name, String adress, String ownerName, List<Room> rooms) {
        this.surface = surface;
        this.name = name;
        this.adress = adress;
        this.ownerName = ownerName;
        this.rooms = rooms;
    }

    public Flat(int surface, String name, String ownerName) {
        this.surface = surface;
        this.name = name;
        this.ownerName = ownerName;
    }

    @Override
    public String toString() {
        return "Flat{" +
                "id=" + id +
                ", surface=" + surface +
                ", name='" + name + '\'' +
                ", adress='" + adress + '\'' +
                ", ownerName='" + ownerName + '\'' +
                ", rooms=" + rooms +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSurface() {
        return surface;
    }

    public void setSurface(int surface) {
        this.surface = surface;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }
}
