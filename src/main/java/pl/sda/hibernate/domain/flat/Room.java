package pl.sda.hibernate.domain.flat;

import javax.persistence.*;

@Entity
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private int doorCount;
    @ManyToOne
    @JoinColumn(name = "FLAT_ID")//mozemy to nazwac jak chcemy bo to pole FLAT_ID bedzie teraz utworzone
    private Flat flat;

    public Room() {
    }

    public Room(String name, int doorCount, Flat flat) {
        this.name = name;
        this.doorCount = doorCount;
        this.flat = flat;
    }

    @Override
    public String toString() {
        return "Room{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", doorCount=" + doorCount +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDoorCount() {
        return doorCount;
    }

    public void setDoorCount(int doorCount) {
        this.doorCount = doorCount;
    }

    public Flat getFlat() {
        return flat;
    }

    public void setFlat(Flat flat) {
        this.flat = flat;
    }
}
