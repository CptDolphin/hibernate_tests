package pl.sda.hibernate.domain;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Address implements Serializable {

	private String country;
	private String town;
	private String street;

	public String getCountry() {
		return country;
	}

	public String getTown() {
		return town;
	}

	public String getStreet() {
		return street;
	}

	public Address() {
	}

	public Address(String country, String town, String street) {
		this.country = country;
		this.town = town;
		this.street = street;
	}

	@Override
	public String toString() {
		return "Address{" +
				"country='" + country + '\'' +
				", town='" + town + '\'' +
				", street='" + street + '\'' +
				'}';
	}
}
