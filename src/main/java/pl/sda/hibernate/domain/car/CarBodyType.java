package pl.sda.hibernate.domain.car;

public enum CarBodyType {
    SEDAN,
    COMBI,
    CABRIO
}
