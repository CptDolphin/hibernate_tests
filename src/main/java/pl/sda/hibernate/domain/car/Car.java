package pl.sda.hibernate.domain.car;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Entity
public class Car {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;
    private String brand;
    private String model;
    @Enumerated(EnumType.ORDINAL)
    private CarBodyType carBodyType;
    //CarBodyType.valueof();
    private LocalDate productionDate;
    @Transient
    private int age;
    private String color;
    private long km;
    @PrePersist
    @PreUpdate
    public void modifiedDate(){
        System.out.println("Modifying LocalDateTime in Car");
        modifiedDate = LocalDateTime.now();
    }
    private LocalDateTime modifiedDate;

    public Car(String brand, String model, LocalDate productionDate, int age, String color, long km) {
        this.brand = brand;
        this.model = model;
        this.carBodyType = CarBodyType.SEDAN;
        this.productionDate = productionDate;
        this.age = age;
        this.color = color;
        this.km = km;
        modifiedDate();
    }

    public Car() {
    }

    @PostLoad
    public void calculateAge(){
        System.out.println("Wykonuje sie metoda 'calculateAge' w encji");
        age = Math.toIntExact(ChronoUnit.YEARS.between(productionDate, LocalDateTime.now()));
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", carBodyType=" + carBodyType +
                ", productionDate=" + productionDate +
                ", age=" + age +
                ", color='" + color + '\'' +
                ", km=" + km +
                '}';
    }

    public LocalDateTime getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(LocalDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public CarBodyType getCarBodyType() {
        return carBodyType;
    }

    public void setCarBodyType(CarBodyType carBodyType) {
        this.carBodyType = carBodyType;
    }

    public LocalDate getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(LocalDate productionDate) {
        this.productionDate = productionDate;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public long getKm() {
        return km;
    }

    public void setKm(long km) {
        this.km = km;
    }
}
