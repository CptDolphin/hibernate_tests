package pl.sda.hibernate.domain;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
public class Note {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	private String title;

	private Date rowDate;

	@Temporal(TemporalType.DATE)
	private Date dateAsDate;

	@Temporal(TemporalType.TIME)
	private Date dateAsTime;

	@Temporal(TemporalType.TIMESTAMP)
	private Date dateAsTimestamp;

	private LocalDate localDate;

	private LocalDateTime localDateTime;

	public Note() {
	}

	public Note(String title, Date rowDate, Date dateAsDate, Date dateAsTime, Date dateAsTimestamp, LocalDate localDate, LocalDateTime localDateTime) {
		this.title = title;
		this.rowDate = rowDate;
		this.dateAsDate = dateAsDate;
		this.dateAsTime = dateAsTime;
		this.dateAsTimestamp = dateAsTimestamp;
		this.localDate = localDate;
		this.localDateTime = localDateTime;
	}

	@Override
	public String toString() {
		return "Note{" +
				"id=" + id +
				", title='" + title + '\'' +
				", rowDate=" + rowDate +
				", dateAsDate=" + dateAsDate +
				", dateAsTime=" + dateAsTime +
				", dateAsTimestamp=" + dateAsTimestamp +
				", localDate=" + localDate +
				", localDateTime=" + localDateTime +
				'}';
	}
}
