package pl.sda.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import pl.sda.hibernate.dao.SchoolDao;
import pl.sda.hibernate.util.HibernateUtil;

public class SchoolDaoDemo {
    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
    Session session = sessionFactory.openSession();
    SchoolDao schoolDao = new SchoolDao(session);

}
