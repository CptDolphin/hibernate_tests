package pl.sda.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.hibernate.domain.Employee;
import pl.sda.hibernate.domain.User;
import pl.sda.hibernate.domain.flat.Flat;
import pl.sda.hibernate.domain.flat.Room;
import pl.sda.hibernate.util.HibernateUtil;

public class HibernateDemo {

	public static void main(String[] args) {

		SessionFactory sessionFactory = null;
		Session session = null;
		Transaction transaction = null;

		try {
			sessionFactory = HibernateUtil.getSessionFactory();
			session = sessionFactory.openSession();
			transaction = session.beginTransaction();

//			Flat flat = new Flat(90,"Szczurek","Szczurek");
//			session.persist(flat);

//			Flat flat = session.find(Flat.class,1);
//			Room salon = new Room("salon",1,flat);
//			session.persist(salon);
//

			Flat flat = session.find(Flat.class,1);
			System.out.println("flat = " + flat);
// create
//			Employee employee = new Employee("Rafal","tajneHaslo",21);
//			session.persist(employee);

//			read
//			for (int i = 0; i < 10000; i++) {
//				Employee employee = session.find(Employee.class, 2);
//				System.out.println("employee = " + employee);
//			}

//			update
//			employee.setPassword("asdasd");

//			delete
//			session.delete(employee);
//			System.out.println("employee = " + employee);

			transaction.commit();
		} catch (Exception e) {
			transaction.rollback();
		} finally {
			session.close();
			sessionFactory.close();
		}

	}
}
