package pl.sda.hibernate.relations.one2one;

import javax.persistence.*;

@Entity
public class Human {

	@Id
	private int id;
	private String name;
	@OneToOne
	@PrimaryKeyJoinColumn
	private Brain brain;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Brain getBrain() {
		return brain;
	}

	public void setBrain(Brain brain) {
		this.brain = brain;
	}

	@Override
	public String toString() {
		return "Human{" +
				"id=" + id +
				", name='" + name + '\'' +
				", brain=" + brain +
				'}';
	}
}
