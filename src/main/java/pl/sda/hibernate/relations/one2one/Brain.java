package pl.sda.hibernate.relations.one2one;

import javax.persistence.*;

@Entity
public class Brain {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private int size;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	@Override
	public String toString() {
		return "Brain{" +
				"id=" + id +
				", size=" + size +
				'}';
	}
}
