package pl.sda.hibernate.relations;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import pl.sda.hibernate.relations.Message;
import pl.sda.hibernate.util.HibernateUtil;

import java.util.Date;
import java.util.List;

public class MessageDAO {

	private final SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

	public Message create(String title, String body, Date createdDate, boolean read, String from, String to, Message.MessageType type) {
		Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();

			Message message = new Message(title, body, createdDate, read, from, to, type);
			session.persist(message);

			session.getTransaction().commit();
			return message;
		} catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return null;
	}

	public Message read(int id) {
		Session session = sessionFactory.openSession();
		return session.find(Message.class, id);
	}

	public List<Message> findAll() {
		Session session = sessionFactory.openSession();
		List<Message> messages = session.createQuery("from Message").getResultList();
		return messages;
	}

	public List<Message> findAllByRead(boolean read) {
		Session session = sessionFactory.openSession();
		List<Message> messages = session
				.createQuery("from Message where read = :read")
				.setParameter("read", read)
				.getResultList();
		return messages;
	}

	public List<Message> findAllByMessageType(Message.MessageType type) {
		Session session = sessionFactory.openSession();
		List<Message> messages = session
				.createQuery("from Message where type = :type")
				.setParameter("type", type)
				.getResultList();
		return messages;
	}

	public List<Message> findAllReadByMessageType(Message.MessageType type) {
		Session session = sessionFactory.openSession();
		List<Message> messages = session
				.createQuery("from Message where type = :type AND read = :read")
				.setParameter("type", type)
				.setParameter("read", true)
				.getResultList();
		return messages;
	}

	public List<Message> findAllByMessageTypeOrderByCreatedDesc(Message.MessageType type) {
		Session session = sessionFactory.openSession();
		List<Message> messages = session
				.createQuery("from Message where type = :type ORDER BY created DESC")
				.setParameter("type", type)
				.getResultList();
		return messages;
	}

	public List<Message> findAll(int limit) {
		Session session = sessionFactory.openSession();
		List<Message> messages = session
				.createQuery("from Message")
				.setMaxResults(limit)
				.getResultList();
		return messages;
	}

	// dla chętnych: public Message update(Message message)
	public Message update(int id, boolean read, Message.MessageType type) {
		Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();

			Message message = session.find(Message.class, id);
			if (message != null) {
				message.setRead(read);
				message.setType(type);
			}

			session.getTransaction().commit();
			return message;
		} catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return null;
	}

	public void delete(int id) {
		Session session = sessionFactory.openSession();

		try {
			session.beginTransaction();

			Message message = session.find(Message.class, id);
			if (message != null) {
				session.remove(message);
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
}
