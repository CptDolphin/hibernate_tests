package pl.sda.hibernate.relations;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "MESSAGE")
public class Message {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;
	@Column(name = "TITLE")
	private String title;
	@Column(name = "MESSAGE_TEXT")
	private String body;
	@Column(name = "CREATED_DATE")
	@Temporal(TemporalType.TIMESTAMP) // !!!!!!
	private Date created;
	@Column(name = "IS_READ")
	private boolean read;
	@Column(name = "WRITER")
	private String from;
	@Column(name = "READER")
	private String to;
	@Enumerated(EnumType.ORDINAL)  // !!!!!!!
	@Column(name = "TYPE")
	private MessageType type;

	public enum MessageType {SMS, MMS;}

	public Message() {
	}

	public Message(String title, String body, Date created, boolean read, String from, String to, MessageType type) {
		this.title = title;
		this.body = body;
		this.created = created;
		this.read = read;
		this.from = from;
		this.to = to;
		this.type = type;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public void setRead(boolean read) {
		this.read = read;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public void setType(MessageType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Message{" +
				"id=" + id +
				", title='" + title + '\'' +
				", body='" + body + '\'' +
				", created=" + created +
				", read=" + read +
				", from='" + from + '\'' +
				", to='" + to + '\'' +
				", type=" + type +
				'}';
	}
}
