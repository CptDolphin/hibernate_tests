package pl.sda.hibernate.relations;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import pl.sda.hibernate.util.HibernateUtil;

public class MessageMain {
	public static void main(String[] args) {
		SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
		Session session = sessionFactory.openSession();

		Message message = session.find(Message.class, 1);
		System.out.println("message = " + message);

		session.close();
		sessionFactory.close();
	}
}
