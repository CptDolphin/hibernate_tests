package pl.sda.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import pl.sda.hibernate.dao.CarDao;
import pl.sda.hibernate.util.HibernateUtil;

public class CarDaoDemo {
    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();
        CarDao carDao = new CarDao(session);
//        carDao.searchByOrder("color").forEach(System.out::println);
        carDao.searchByOrder("km").forEach(System.out::println);
    }
}




/*
while (isWorking) {
                System.out.println("create/");
                String line = scanner.nextLine();
                String[] splits = line.split(" ");
                if (splits[0].equalsIgnoreCase("create")) {
                    String brand = splits[1];
                    String model = splits[2];
                    LocalDate productionDate= LocalDate.of(Integer.parseInt(splits[3]),Integer.parseInt(splits[4]),Integer.parseInt(splits[5]));
                    int age = Integer.parseInt(splits[6]);
                    String color = splits[7];
                    Long km = Long.valueOf(splits[8]);
                    carDao.create(brand, model, productionDate, age, color, km);
                }else if(splits[0].equalsIgnoreCase("findbyid")){
                    System.out.println(carDao.findById(Integer.parseInt(splits[1])));
                }else if(line.equalsIgnoreCase("change")){
//                    if(splits[1].equalsIgnoreCase("marka")){
//                        carDao.changeCarAge(Integer.parseInt(splits[2]));
//                    }
                }else if(line.equalsIgnoreCase("findall")){
                    carDao.readAllCars();
                }else if(line.equalsIgnoreCase("quit")){
                    System.exit(0);
                }
            }
 */