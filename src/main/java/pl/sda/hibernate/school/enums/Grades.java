package pl.sda.hibernate.school.enums;

public enum Grades {
    TWO(2.0),
    TWOPLUS(2.5),
    THREE(3.0),
    THREEPLUS(3.5),
    FOUR(4.0),
    FOURPLUS(4.5),
    FIVE(5.0),
    FIVEPLUS(5.5),
    SIX(6.0);

    Double grade;

    Grades(Double grade) {
        this.grade = grade;
    }

    public Double getGrade() {
        return grade;
    }

    public void setGrade(Double grade) {
        this.grade = grade;
    }
}
