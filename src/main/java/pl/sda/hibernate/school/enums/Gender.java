package pl.sda.hibernate.school.enums;

public enum Gender {
    MALE,
    FEMALE,
    HELICOPTER;
}
