package pl.sda.hibernate.school.entities;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.time.LocalDate;

@Entity
public class Activity {
    @Id
    private int id;
    @ManyToOne
    private SchoolClass schoolClass;
    @OneToOne
    private Subject subject;
    @OneToOne
    private Teacher teacher;
    @OneToOne
    private Room room;
    private LocalDate dateOfActivity;
}
