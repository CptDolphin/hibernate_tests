package pl.sda.hibernate.school.entities;

import pl.sda.hibernate.school.enums.Gender;

import javax.persistence.*;
import java.util.List;

@Entity
public class Teacher {
    @Id
    private int id;
    private String name;
    private String surname;
    @Enumerated(EnumType.ORDINAL)
    private Gender gender;
    @OneToOne
    @JoinColumn(name = "SCHOOLCLASS_ID")
    private SchoolClass schoolClass;
    @ManyToOne
    @JoinColumn(name = "SUBJECT_ID")
    private List<Subject> subjects;

}
