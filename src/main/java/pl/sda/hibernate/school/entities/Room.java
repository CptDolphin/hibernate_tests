package pl.sda.hibernate.school.entities;

import pl.sda.hibernate.school.enums.ClassLevel;
import pl.sda.hibernate.school.enums.Deck;

import javax.persistence.*;

@Entity
public class Room {
    @Id
    private int id;
    @OneToOne
    private ClassLevel classNumber;
    @Enumerated(EnumType.ORDINAL)
    private Deck deck;
}
