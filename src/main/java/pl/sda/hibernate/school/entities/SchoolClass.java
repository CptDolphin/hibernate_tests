package pl.sda.hibernate.school.entities;

import pl.sda.hibernate.school.enums.ClassLevel;

import javax.persistence.*;
import java.util.List;

@Entity
public class SchoolClass {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String classNumber;
    @Enumerated (EnumType.ORDINAL)
    private ClassLevel classLevel;
    private int year;
    @OneToOne (mappedBy = "schoolClass")
    private Teacher educatorId;
    private Student presidentId;
    private List<Student> students;
}
