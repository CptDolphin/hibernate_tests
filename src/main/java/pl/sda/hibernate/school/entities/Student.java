package pl.sda.hibernate.school.entities;

import pl.sda.hibernate.school.enums.Gender;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String surname;
    @Enumerated(EnumType.ORDINAL)
    private Gender gender;
    private LocalDate birthDate;
    @ManyToOne
    @JoinColumn(name = "SCHOOLCLASS_ID")
    private SchoolClass schoolClass;
}
