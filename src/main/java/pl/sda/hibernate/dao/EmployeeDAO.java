package pl.sda.hibernate.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.sda.hibernate.domain.Employee;

import javax.persistence.Id;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class EmployeeDAO {
	@Id
	int id;
	private Session session;

	public EmployeeDAO(Session session) {
		this.session = session;
	}

	public Employee create(String name, String password, int age) {
		Transaction transaction = session.beginTransaction();

		Employee employee = new Employee(name, password, age);
		session.persist(employee);

		transaction.commit();
		return employee;
	}

	public Employee getById(int id) {
		return session.find(Employee.class, id);
	}

	public Optional<Employee> findById(int id) {
		return session.byId(Employee.class).loadOptional(id);
	}

	public List<Employee> findAll() {
		List<Employee> list = new ArrayList<>();
		list = session.createQuery("from employee").getResultList();
		if(list != null){
			return list;
		}
		return Collections.emptyList();
	}

	public Employee update(Employee updatedEmployee) {
		Transaction transaction = session.beginTransaction();
		Employee employee = session.find(Employee.class, updatedEmployee.getId());
		employee = changePassword(updatedEmployee.getId(),employee.getPassword(),updatedEmployee.getPassword());
		employee.setName(updatedEmployee.getName());
		employee.setAge(updatedEmployee.getAge());
		//nie wolno zmieniac hasla
		transaction.commit();
		return employee;
	}

	private Employee changePassword(int id, String password, String password1) {
		Transaction transaction = session.beginTransaction();
		if(password==null || password1==null){
			throw new NullPointerException("Missing password to Update");
		}else if (password1.length()<3){
			throw new IllegalArgumentException("Password is to short");
		}else if(password==password1){
			throw new IllegalArgumentException("Password same as last one");
		}
		Employee employee = session.find(Employee.class,id);
		employee.setPassword(password1);
		transaction.commit();
		return employee;
	}

	public Employee delete(int id) {
		Transaction transaction = session.beginTransaction();

		Employee employee = session.find(Employee.class, id);
		session.delete(employee);

		transaction.commit();
		return employee;
	}
}
