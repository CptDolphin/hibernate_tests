package pl.sda.hibernate.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.sda.hibernate.domain.car.Car;
import pl.sda.hibernate.domain.car.CarBodyType;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class CarDao {
    private Session session;

    public CarDao(Session session) {
        this.session = session;
    }

    public Car create(String brand, String model, LocalDate productionYear, int age, String color, long km) {
        Transaction transaction = session.beginTransaction();

        Car car = new Car(brand, model, productionYear, age, color, km);
        session.persist(car);

        transaction.commit();
        return car;
    }

    public Optional<Car> findById(int id) {
        return session.byId(Car.class).loadOptional(id);
    }
    //uzywamy jak chcemy zmienic stan obiektu czyli przy czytaniu nie a przy reszczie tak


    public Car removeCar(int id) {
        Transaction transaction = session.beginTransaction();
        Car car = session.find(Car.class, id);
        session.remove(car);
        transaction.commit();
        return car;
    }

    public List<Car> readAllCars() {
        return session.createQuery("select c from Car c").getResultList();
    }

    public List<Car> searchByKMAsc() {
        return  session.createQuery("select c from Car c order by c.km asc").getResultList();
//        if (list != null) {
//            return list;
//        } else return null;
    }

    public List<Car> searchByKMDesc() {
        List<Car> list = session
                .createQuery("select c from Car c order by c.km desc")
                .getResultList();
        if (list != null) return list;
        else return null;
    }

    public List<Car> searchOldestCar() {
        List<Car> list = session.createQuery("select c from Car c order by productionDate asc limit 1").getResultList();
        if (list != null) {
            return list;
        } else return null;
    }

    public List<Car> searchNewestCar() {
        List<Car> list = session.createQuery("select c from Car c order by productionDate desc limit 1").getResultList();
        if (list != null) {
            return list;
        } else return null;
    }

    /*
        public List<Car> searchByOrder(String brand) {
            return session
                    .createQuery("select c from Car c where brand =:brand")
                    .setParameter("brand", brand)
                    .getResultList();
        }
    */
     //dziala
//    public List<Car> searchByOrder(String order) {
//        return session
//                .createQuery("from Car order by "+order+" asc")
//                .getResultList();
//    }
    public List<Car> searchByOrder(String order) {
        return session
                .createQuery("from Car order by :order asc")
                .setParameter("order",order)
                .getResultList();
    }

    public Car changeCarBrand(Car carUpdated, String brand) {
        Transaction transaction = session.beginTransaction();
        Car car = session.find(Car.class, carUpdated.getId());
        car.setBrand(brand);
        transaction.commit();
        return car;
    }

    public Car changeCarModel(Car carUpdated, String model) {
        Transaction transaction = session.beginTransaction();
        Car car = session.find(Car.class, carUpdated.getId());
        car.setModel(model);
        transaction.commit();
        return car;
    }

    public Car changeCarBodyType(Car carUpdated, CarBodyType bodyType) {
        Transaction transaction = session.beginTransaction();
        Car car = session.find(Car.class, carUpdated.getId());
        car.setCarBodyType(bodyType);
        transaction.commit();
        return car;
    }

    public Car changeCarProductionDate(Car carUpdated, LocalDate productionDate) {
        Transaction transaction = session.beginTransaction();
        Car car = session.find(Car.class, carUpdated.getId());
        car.setProductionDate(productionDate);
        transaction.commit();
        return car;
    }

    public Car changeCarAge(Car carUpdated, int age) {
        Transaction transaction = session.beginTransaction();
        Car car = session.find(Car.class, carUpdated.getId());
        car.setAge(age);
        transaction.commit();
        return car;
    }

    public Car changeCarColor(Car carUpdated, String color) {
        Transaction transaction = session.beginTransaction();
        Car car = session.find(Car.class, carUpdated.getId());
        car.setColor(color);
        transaction.commit();
        return car;
    }


    public Car changeCarKM(Car carUpdated, Long KM) {
        Transaction transaction = session.beginTransaction();
        Car car = session.find(Car.class, carUpdated.getId());
        car.setKm(KM);
        transaction.commit();
        return car;
    }
}
