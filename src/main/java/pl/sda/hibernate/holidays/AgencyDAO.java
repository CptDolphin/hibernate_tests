package pl.sda.hibernate.holidays;

import org.hibernate.Session;

public class AgencyDAO {

	private static final int DEFAULT_COMMISSION = 100;

	private Session session;

	public AgencyDAO(Session session) {
		this.session = session;
	}

	/**
	 * Metoda pobiera wartosc prowizji da danego biura.
	 * Nazwy agencji przechowywane sa wielkimi literami.
	 * Obsluz sytuacje, gdy klient poda nazwe malymi.
	 * Jesli klient poda nieistniejaca agencje, to zwracamy DEFAULT_COMMISSION
	 *
	 * @param agencyName - nazwa agencji
	 * @return - prowizja agencji
	 */
	public int getCommissionByAgency(String agencyName) {
		// TODO:
		return 0;
	}
}
