package pl.sda.hibernate.holidays;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Travel {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int id;
    private String source;
    private String destination;
    private LocalDate startDate;
    private LocalDate endDate;
    private int distance;
    @Transient
    @PrePersist
    @PostUpdate
    public int distance(){
        System.out.println("Modyfying distance");
        return (source.length() + destination.length())*10;
    }
    private int adults;
    private int children;

    public Travel() {
    }

    public Travel(String source, String destination, LocalDate startDate, LocalDate endDate, int adults, int children) {
        this.source = source;
        this.destination = destination;
        this.startDate = startDate;
        this.endDate = endDate;
        this.adults = adults;
        this.children = children;
        this.distance = distance();
    }

    @Override
    public String toString() {
        return "Travel{" +
                "id=" + id +
                ", source='" + source + '\'' +
                ", destination='" + destination + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", distance=" + distance +
                ", adults=" + adults +
                ", children=" + children +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getAdults() {
        return adults;
    }

    public void setAdults(int adults) {
        this.adults = adults;
    }

    public int getChildren() {
        return children;
    }

    public void setChildren(int children) {
        this.children = children;
    }
}
