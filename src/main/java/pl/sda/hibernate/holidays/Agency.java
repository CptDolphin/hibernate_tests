package pl.sda.hibernate.holidays;

import javax.persistence.Entity;
import javax.persistence.Id;


/**
 * Encja reprezentujaca agencje/tour operatora/biuro podrozy.
 * Kluczem encji jest nazwa (name).
 * Tabela przechowuje pole commission - wartosc prowizji danego biura.
 */
@Entity
public class Agency {
    @Id
    private String name;

	// TODO: zaimplementuj klase, zaprojektuj pola i metody
}
