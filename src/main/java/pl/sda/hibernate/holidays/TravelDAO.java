package pl.sda.hibernate.holidays;

import org.hibernate.Session;
import org.hibernate.Transaction;

import java.time.LocalDate;

public class TravelDAO {

	private Session session;

	public TravelDAO(Session session) {
		this.session = session;
	}

	public Travel create(String source, String destination, LocalDate startDate, LocalDate endDate, int adults, int children){
		Transaction transaction = session.beginTransaction();

		Travel travel = new Travel(source,destination,startDate,endDate,adults,children);
		session.persist(travel);

		transaction.commit();
		return travel;
	}

	// TODO: zaimplementuj ponizsze metody
	// findById

	// update

	// delete

	// lista wycieczek posortowana wg daty startu

	// lista wycieczek posortowana wg dlugosci (ilosci dni)

}
