package pl.sda.hibernate.holidays;

import java.time.LocalDate;

/**
 * Klasa przechowuje parametry wycieczki, ktora chcemy zamowic
 */
public class TravelParams {

	private String from;
	private String to;
	private LocalDate startDate;
	private LocalDate endDate;
	private int adults;
	private int children;
	/**
	 * Nazwa agencji turystycznej, w ktorej chcemy zakupic wycieczke
	 */
	private String travelAgency;


	public TravelParams(String from, String to, LocalDate startDate, LocalDate endDate, int adults, int children, String travelAgency) {
		this.from = from;
		this.to = to;
		this.startDate = startDate;
		this.endDate = endDate;
		this.adults = adults;
		this.children = children;
		this.travelAgency = travelAgency;
	}

	public String getFrom() {
		return from;
	}

	public String getTo() {
		return to;
	}

	public LocalDate getStartDate() {
		return startDate;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public int getAdults() {
		return adults;
	}

	public int getChildren() {
		return children;
	}

	public String getTravelAgency() {
		return travelAgency;
	}

	@Override
	public String toString() {
		return "TravelParams{" +
				"from='" + from + '\'' +
				", to='" + to + '\'' +
				", startDate=" + startDate +
				", endDate=" + endDate +
				", adults=" + adults +
				", children=" + children +
				", travelAgency='" + travelAgency + '\'' +
				'}';
	}
}
